#!/bin/sh
# fab@clacks.xyz
#
# needs the following pkgs
# libnotify
# ImageMagick
# i3lock

resume() {
    notify-send DUNST_COMMAND_RESUME
    xset dpms 0 0 0
    exit 0
}

create_lockimage() {
    convert ~/.theme/wallpaper \
        -level "0%,100%,0.6" \
        -filter Gaussian \
        -resize 20% \
        -define "filter:sigma=1.5" \
        -resize 500.5% \
        -font "Tamsyn" \
        -pointsize 20 \
        -fill "white" \
        -gravity center -annotate -0+120 "Type password to unlock" \
        ~/.theme/lock.png \
        -gravity center \
        -composite ~/.theme/lockscreen.png
    exit 0
}

[[ "X$1" == "Xcreate" ]] && create_lockimage

trap resume HUP INT TERM
notify-send DUNST_COMMAND_PAUSE
xset +dpms dpms 5 5 5
xset +dpms dpms force off
i3lock -n -e -i ~/.theme/lockscreen.png >/dev/null 2>&1
resume
